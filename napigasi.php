<!-- banner -->
<div class="main_section_agile" id="home">
	<div class="agileits_w3layouts_banner_nav">
		<nav class="navbar navbar-default">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<h1><a class="navbar-brand" href="index.php"><i class="fa fa-leanpub" aria-hidden="true"></i> SEKOLAH ILHAM </a></h1>

			</div>
			<div class="w3layouts_header_right ">
			    <form action="#" method="post">
					
					
				</form>
			</div>
			
			<?php
			session_start();
			if (empty($_SESSION['username']))
			{
				
			}
			else
			{
				if ($_SESSION['level'] == "admin")
				{
				echo "
						
						<ul class='agile_forms'>
							<div class='dropdown'>
							<button class='btn btn-success dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							$_SESSION[username] <span class='caret'></span>
							</button>
							<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
							<table class='table'>
								<tr>
									<td><a class='dropdown-item' href='Admin'>Menu Admin</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/akun.php'>Akun</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/murid.php'>Murid</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/guru.php'>Guru</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/Nilai.php'>Nilai</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/pesan.php'>Email</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='Admin/mata_pelajaran.php'>Mata Pelajaran</a></td>
								</tr>
																
								<tr>
									<td><a class='dropdown-item' href='logout.php'>Logout</a></td>
								</tr>
							</table>
							</div>
							</div>
						</ul>
						
					 ";
				}
				else if ($_SESSION['level'] == "guru")
				{
				echo "
						
						<ul class='agile_forms'>
							<div class='dropdown'>
							<button class='btn btn-success dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							$_SESSION[username] <span class='caret'></span>
							</button>
							<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
							<table class='table'>
								<tr>
									<td><a class='dropdown-item' href='Guru'>Input Nilai</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='logout.php'>Logout</a></td>
								</tr>
							</table>
							</div>
							</div>
						</ul>
						
					 ";
				}
				else
				{
				echo "
						
						<ul class='agile_forms'>
							<div class='dropdown'>
							<button class='btn btn-success dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							$_SESSION[username] <span class='caret'></span>
							</button>
							<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
							<table class='table'>
								<tr>
									<td><a class='dropdown-item' href='Murid'>Cek Nilai</a></td>
								</tr>
								
								<tr>
									<td><a class='dropdown-item' href='logout.php'>Logout</a></td>
								</tr>
							</table>
							</div>
							</div>
						</ul>
						
					 ";
				}
				
			}
			?>
			
			
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<nav class="link-effect-2" id="link-effect-2">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php" class="effect-3">Beranda</a></li>
						<li> <a class='effect-3 scroll' data-toggle='dropdown'>DATA MURIT<span class='caret'></span> </a>
							<ul class='dropdown-menu'>
								<li><a href='kelas1.php' >KELAS 1</a></li>
								<li><a href='kelas2.php' >KELAS 2</a></li>
								<li><a href='kelas3.php' >KELAS 3</a></li>
							</ul>
						</li>
						<li><a href="guru.php" class="effect-3">DATA GURU</a></li>
						<li>
						<a href="profile_sekolah.php" class="effect-3">Profil Sekolah</a>
						</li>
						
					</ul>
				</nav>
			</div>
		</nav>	
		<div class="clearfix"> </div> 
	</div>
</div>
