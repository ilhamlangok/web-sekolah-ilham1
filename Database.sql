-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jul 2019 pada 10.53
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun`
--

CREATE TABLE `akun` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akun`
--

INSERT INTO `akun` (`username`, `password`, `level`) VALUES
('admin', 'c4ca4238a0b923820dcc509a6f75849b', 3),
('dadang', 'c4ca4238a0b923820dcc509a6f75849b', 3),
('hendika', 'c4ca4238a0b923820dcc509a6f75849b', 2),
('Kirito', 'c4ca4238a0b923820dcc509a6f75849b', 1),
('Naruto', 'c4ca4238a0b923820dcc509a6f75849b', 3),
('Okky', 'c4ca4238a0b923820dcc509a6f75849b', 3),
('roma', 'c4ca4238a0b923820dcc509a6f75849b', 1),
('udin', 'c4ca4238a0b923820dcc509a6f75849b', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `nip` char(10) NOT NULL,
  `nama_guru` varchar(50) DEFAULT NULL,
  `no_hp` varchar(30) NOT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`nip`, `nama_guru`, `no_hp`, `jenkel`, `agama`, `username`) VALUES
('085566665', 'Suyatmi', '08549922554', 'Perempuan', 'Islam', NULL),
('1111111111', 'Hendika', '087987697658', 'Laki-Laki', 'Islam', 'hendika'),
('1111111112', 'Roma Debrian', '087987697659', 'Laki-Laki', 'Islam', 'roma'),
('1111111113', 'Okky', '02147483647', 'Laki-Laki', 'Islam', 'Okky'),
('3453456456', 'Tumini', '08545698459', 'Perempuan', 'Islam', NULL),
('3456456345', 'Hartono', '084569874565', 'Laki-laki', 'Islam', NULL),
('456456567', 'Hartini', '08556119879', 'Perempuan', 'Kristen', NULL),
('4574534232', 'Rusmini', '0855458885789', 'Perempuan', 'Kristen', NULL),
('5645764564', 'Eki Suryadi', '08546685566', 'Laki-laki', 'Islam', NULL),
('6453346', 'Sasuke', '08541268789', 'Laki-laki', 'Islam', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kel1`
--

CREATE TABLE `kel1` (
  `nisn` char(10) NOT NULL,
  `nama_murid` varchar(50) DEFAULT NULL,
  `kota` varchar(45) DEFAULT NULL,
  `jenkel` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `jurusan` varchar(3) DEFAULT NULL,
  `kelas` int(2) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kel1`
--

INSERT INTO `kel1` (`nisn`, `nama_murid`, `kota`, `jenkel`, `agama`, `jurusan`, `kelas`, `username`) VALUES
('1111111111', 'Udin', 'Jakarta', 'Laki-Laki', 'Islam', 'RPL', 10, 'udin'),
('1111111119', 'Kanna Kamui', 'Tokyo', 'Perempuan', 'Hindu', 'AP', 11, NULL),
('1234123215', 'Yasan', 'Banjarmasin', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('1256734563', 'Jaki', 'Bekasi', 'Laki-Laki', 'islam', 'PRL', 12, NULL),
('15232131', 'Uzumaki Kusina', 'Konoha', 'Perempuan', 'Islam', 'AP', 11, NULL),
('3432423429', 'Hendika', 'Bekasi', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('3563345195', 'Jaja Tamanawa', 'Jambi', 'Laki-Laki', 'islam', 'RPL', 10, NULL),
('3563345199', 'Najwa', 'Medan', 'Perempuan', 'islam', 'AK', 11, NULL),
('5555554323', 'Sasa', 'Maria', 'Perempuan', 'Islam', 'AP', 12, NULL),
('6475834759', 'Culain', 'Mataram', 'Laki-Laki', 'Hindu', 'RPL', 12, NULL),
('6666666666', 'Hana', 'Jakarta', 'Perempuan', 'Islam', 'AP', 11, NULL),
('8378449283', 'Uzumaki Naruto', 'Konoha', 'Laki-Laki', 'Islam', 'RPL', 11, NULL),
('8798679869', 'Okky Pras', 'Banten', 'Laki-Laki', 'Islam', 'RPL', 10, 'Okky');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kel2`
--

CREATE TABLE `kel2` (
  `nisn` char(10) NOT NULL,
  `nama_murid` varchar(50) DEFAULT NULL,
  `kota` varchar(45) DEFAULT NULL,
  `jenkel` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `jurusan` varchar(3) DEFAULT NULL,
  `kelas` int(2) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kel2`
--

INSERT INTO `kel2` (`nisn`, `nama_murid`, `kota`, `jenkel`, `agama`, `jurusan`, `kelas`, `username`) VALUES
('1234123215', 'Yono', 'Banjarmasin', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('234456533', 'Beni', 'Surabaya', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('23546565', 'Winasa', 'Monterado', 'Perempuan', 'Islam', 'AP', 11, NULL),
('345356456', 'Mariana', 'Jakarta', 'Perempuan', 'Islam', 'RPL', 10, 'udin'),
('346345765', 'Prasetyo', 'Suramadu', 'Laki-Laki', 'Islam', 'RPL', 10, 'Okky'),
('3563345195', 'Rizki', 'Jambi', 'Laki-Laki', 'islam', 'RPL', 10, NULL),
('5555554323', 'Tina', 'Palu', 'Perempuan', 'Islam', 'AP', 12, NULL),
('645345657', 'Sarminah', 'Jakarta', 'Perempuan', 'Islam', 'AP', 11, NULL),
('6475834759', 'Culain', 'Mataram', 'Laki-Laki', 'Hindu', 'RPL', 12, NULL),
('674553434', 'Sihabun', 'Medan', 'Laki-laki', 'islam', 'AK', 11, NULL),
('75343454', 'Hinata', 'Malang', 'Perempuan', 'Hindu', 'AP', 11, NULL),
('764534563', 'Boruto', 'Konoha', 'Laki-Laki', 'Islam', 'RPL', 11, NULL),
('765442457', 'Joko', 'Jakarta', 'Laki-Laki', 'islam', 'PRL', 12, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kel3`
--

CREATE TABLE `kel3` (
  `nisn` char(10) NOT NULL,
  `nama_murid` varchar(50) DEFAULT NULL,
  `kota` varchar(45) DEFAULT NULL,
  `jenkel` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `jurusan` varchar(3) DEFAULT NULL,
  `kelas` int(2) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kel3`
--

INSERT INTO `kel3` (`nisn`, `nama_murid`, `kota`, `jenkel`, `agama`, `jurusan`, `kelas`, `username`) VALUES
('1111111111', 'Udin', 'Jakarta', 'Laki-Laki', 'Islam', 'RPL', 10, 'udin'),
('1111111119', 'Kanna Kamui', 'Tokyo', 'Perempuan', 'Hindu', 'AP', 11, NULL),
('1234123215', 'Yasan', 'Banjarmasin', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('1256734563', 'Jaki', 'Bekasi', 'Laki-Laki', 'islam', 'PRL', 12, NULL),
('15232131', 'Uzumaki Kusina', 'Konoha', 'Perempuan', 'Islam', 'AP', 11, NULL),
('3432423429', 'Hendika', 'Bekasi', 'Laki-Laki', 'Islam', 'RPL', 10, NULL),
('3563345195', 'Jaja Tamanawa', 'Jambi', 'Laki-Laki', 'islam', 'RPL', 10, NULL),
('3563345199', 'Najwa', 'Medan', 'Perempuan', 'islam', 'AK', 11, NULL),
('5555554323', 'Sasa', 'Maria', 'Perempuan', 'Islam', 'AP', 12, NULL),
('6475834759', 'Culain', 'Mataram', 'Laki-Laki', 'Hindu', 'RPL', 12, NULL),
('6666666666', 'Hana', 'Jakarta', 'Perempuan', 'Islam', 'AP', 11, NULL),
('8378449283', 'Uzumaki Naruto', 'Konoha', 'Laki-Laki', 'Islam', 'RPL', 11, NULL),
('8798679869', 'Okky Pras', 'Banten', 'Laki-Laki', 'Islam', 'RPL', 10, 'Okky');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`),
  ADD KEY `guru_ibfk_1` (`username`);

--
-- Indeks untuk tabel `kel1`
--
ALTER TABLE `kel1`
  ADD PRIMARY KEY (`nisn`),
  ADD KEY `murid_ibfk_1` (`username`);

--
-- Indeks untuk tabel `kel2`
--
ALTER TABLE `kel2`
  ADD PRIMARY KEY (`nisn`),
  ADD KEY `murid_ibfk_1` (`username`);

--
-- Indeks untuk tabel `kel3`
--
ALTER TABLE `kel3`
  ADD PRIMARY KEY (`nisn`),
  ADD KEY `murid_ibfk_1` (`username`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`username`) REFERENCES `akun` (`username`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
